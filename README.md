## Command-line utility for recursive defragmentation of Btrfs file system.

It checks for number of extents using `filefrag` utility and uses
pre-configured threshold to decide whether to proceed with defragmentation.

[btrfs-defrag](https://gitlab.com/onlyjob/btrfs-defrag/raw/master/btrfs-defrag.sh)
shows number of extents before and after defragmentation.
