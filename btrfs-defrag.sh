#!/bin/bash
: <<=cut

=head1 NAME

btrfs-defrag.sh -- recursive brtfs defragmentation

=head1 SYNOPSYS

btrfs-defrag.sh [ B<-t N> ] [ B<-n> ] [ B<-0> ] B<directory>

=head1 OPTIONS

=over 4

=item B<-0>

Dry-run, do not defragment, merely report fragmentation above threshold level.

=item B<-c[zlib|lzo]>

Compress files while defragmenting.

=item B<-h>, B<--help>

Show usage and options.

=item B<-n>

Unconditional defragmentation without counting for number of extents.

=item B<-t 50>

Threshold, minimum number of extents in directory to defragment.

=back

=head1 DESCRIPTION

Btrfs defragentation, recursive.

=head1 COPYRIGHT

Copyright: 2013-2014 Libre Solutions Pty Ltd (http://raid6.com.au)
Copyright: 2012-2014 Dmitry Smirnov <onlyjob@member.fsf.org>

=head1 LICENSE

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

=head1 ACKNOWLEDGEMENTS

Thanks to
  L<"Me, BSD" (blog)|http://mebsd.com/coding-snipits/bash-spinner-example-freebsd-loading-spinner.html>
for implementation of bash spinner.

=head1 DONATIONS

 Donations are much appreciated, please consider donating:

   Bitcoins : 15nCM6Rs4zoQKhBV55XxcPfwPKeAEHPbAn
  Litecoins : LZMLDNSfy3refx7bKQtEvPyYSbNHsfzLRZ
 AUD/PayPal : https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=onlyjob%40gmail%2ecom&lc=AU&currency_code=AUD

=head1 AVAILABILITY

  Home page : https://gitlab.com/onlyjob/btrfs-defrag
     Script : https://gitlab.com/onlyjob/btrfs-defrag/raw/master/btrfs-defrag.sh

=head1 VERSION

2014.0324

=head1 CHANGELOG

2014.0324
  * Supressed warning when 'dpkg' is not available.

2014.0309
  * Added warning if outdated "e2fsprogs" detected.

=cut

## spinner
i=1;
sp="/-\|";

C_GREEN=$(tput setaf 2)
C_RED=$(tput setaf 1)
C_TEAL=$(tput setaf 6)
T_RESET=$(tput sgr0)$(tput setaf 7)
T_SEP=${C_TEAL}::${T_RESET}
declare -i THR=0
declare -i TLE=0
declare -i DE=0
declare -i LE=0

while [ -n "$1" ]; do
    if [ "$1" = "-h" -o "$1" = "--help" ]; then
        [ $(which perldoc) ] && perldoc "$0" \
        || grep --before-context=999 --regexp '^=cut$' "$0"
        exit 0
    elif [ "$1" = "-n" ]; then
        O_NOFILEFRAG=1
    elif [ "$1" = "-0" ]; then
        O_NODEFRAG=1
    elif [ "$1" = "-t" ]; then
        shift
        THR=$1
        if [ ${THR} -eq 0 ]; then
            echo "E: Threshold must be number greater than zero." 1>&2
            exit 1
        fi
        continue
    elif [ "$1" = "-clzo" ]; then
        BTRFS_O="$1"
    elif [ "$1" = "-czlib" ]; then
        BTRFS_O="$1"
    elif [ ! "${1:0:1}" = "-" ]; then
        DIR="$1"
    else
        echo -e "E: unknown option. Usage:" 1>&2
        echo "    $(basename $0) [ -t THRESHOLD ] [ -n ] [ -0 ] directory"
        exit 1
    fi
    shift
done

[ ${THR} -eq 0 ] && THR=50        # set default threshold
#echo "I: extents threshold=${THR}"

## check for privileges (id -u)
if [ $EUID -ne 0 ]; then
    echo "E: defragmentation needs root priviledges." 1>&2
    exit 1
fi

if [ ! -d "${DIR}" ]; then
    echo "E: need directory to defragment, use '-h' for help." 1>&2
    exit 1
fi

## check for 'filefrag' availability.
filefrag 2>/dev/null
if [ $? -eq 127 ] && [ "${O_NOFILEFRAG}" != "1" ]; then
    echo "E: 'filefrag' not found, please install 'e2fsprogs'." 1>&2
    exit 1
fi

## check e2fsprogs version (older 'filefrag' could stuck on some files)
if dpkg --compare-versions $(dpkg --status e2fsprogs 2>/dev/null | grep Version | cut -d' ' -f2) lt 1.42.9 2>/dev/null; then
    echo "W: consider upgrading 'e2fsprogs' (installed version may cause problems)."
fi


## check for 'btrfs' availability.
btrfs version >/dev/null
if [ $? -eq 127 ] && [ "${O_NODEFRAG}" != "1" ]; then
    echo "E: 'btrfs' not found, please install 'btrfs-tools'." 1>&2
    exit 1
fi

if [ "${O_NOFILEFRAG}" = "1" ] && [ "${O_NODEFRAG}" = "1" ]; then
    echo "E: meaningless options: nothing to do." 1>&2
    exit 1
fi

shopt -s dotglob        ## '*' will match hidden files (i.e. file names starting with '.').

## count fragmented extents in directory.
_count-extents () {
    ## 0 extents means non file (i.g. directory or special file); 1 extent is non-fragmented file.
    [ "${O_NOFILEFRAG}" = "1" ] \
    || filefrag "$1"/* 2>/dev/null | perl -0ne 'my $n=0; $n+=$1-($1>=1 ? 1 : 0) while m{:\s+(\d+)\s+extents?\s+found}g; print int($n);'
}

## defrag file if total number of extents in directory is less than threshold.
_defrag () {
    DE=$(_count-extents "$1")

    if [ "${DE}" -gt ${THR} ] || [ "${O_NOFILEFRAG}" = "1" ]; then
        # defragment
        echo -n "$1"
        [ "${O_NOFILEFRAG}" = "1" ] || echo -n " ${T_SEP} ${DE} extents"
        [ "${O_NODEFRAG}" = "1" ] || btrfs fi defrag -f ${BTRFS_O} "$1" "$1"/* 2>/dev/null
        LE=$(_count-extents "$1")
        if [ ${DE} -eq ${LE} ]; then
            echo ""
        else
            TLE=$((${TLE}+${DE}-${LE}))
            local MSG_COLOR
            if [ ${DE} -gt ${LE} ]; then
                MSG_COLOR="${C_GREEN}"
            else
                MSG_COLOR="${C_RED}"
            fi
            echo "${MSG_COLOR} (less $(( ${DE} - ${LE} ))) ${T_RESET}= ${LE}"
        fi
    fi
}

## recursively defrag of given directory.
_defrag_tree () {
    while IFS= read -d '' D; do
        printf "%s\b" "${sp:i++%${#sp}:1}"       # spinner
        _defrag "${D}"
    done < <(find "$1" -xdev -type d -print0)
}

## good to go for defrag

print_summary () {
    if [ "${TLE}" -gt 0 ]; then
        echo "${T_SEP} ${C_GREEN}${TLE}${T_RESET} extents eliminated."
    elif [ "${TLE}" -lt 0 ]; then
        echo "${T_SEP} ${C_RED}${TLE#-}${T_RESET} more extents after defragmentation (due to compression?)."
    fi
}

trap "print_summary; exit" INT

## main()
_defrag "${DIR}"        ## defrag top level directory
## defrag 1 level dirs, excluding top level dir.
while IFS= read -d '' TD; do
    _defrag_tree "${TD}"
done < <(find "${DIR}" -maxdepth 1 -xdev -type d ! -path "${DIR}" -print0 | sort -z)

print_summary
